﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasinoProject
{
    class CasinoCalculator : Casino                                                                      //класс вычисляет победу игрока
    {
        public static int bet { get; set; } = 0;   //ставка игрока
        public static bool realspin { get; set; } = false; //Реальный ли спин?
        public static int spins { get; set; }   //количество спинов
        //public static int allbets { get; set; } = bet*amountbets; // вся сумма ставок
        public static int CalculateWin()                                                 //метод проверяет победу и возвращает коэффиценты выигрыша
        {
            int win = 0;

            foreach (var selectedNumber in selectedNumbers)
            {
                if (number == selectedNumber)                          //проверка на число
                {

                    win += bet * 37;
                }



             
            }


                if (red && (number == 1 || number == 3 || number == 5 ||    //проверка на красное 
                    number == 7 || number == 9 || number == 12 ||
                    number == 14 || number == 16 || number == 18 ||
                    number == 19 || number == 21 || number == 23 ||
                    number == 25 || number == 27 || number == 30 ||
                    number == 32 || number == 34 || number == 36))
                {

                    win += bet * 2;
                }

                if (black && (number == 2 || number == 4 || number == 6 ||    //проверка на черное 
                     number == 8 || number == 10 || number == 11 ||
                    number == 13 || number == 15 || number == 17 ||
                    number == 20 || number == 22 || number == 24 ||
                    number == 26 || number == 28 || number == 29 ||
                    number == 30 || number == 33 || number == 35))
                {
                    win += bet * 2;
                }


                if (dozen1 && number < 13)                         //проверка на дюжину
                    win += bet * 3;
                if (dozen2 && (number > 12 && number < 25))
                    win += bet * 3;
                if (dozen3 && (number > 24))
                    win += bet * 3;

            if (pair && number % 2 == 0)                             //проверка на четность
            {
                win += bet * 2;
            }

            if (impair && number % 2 != 0)                             //проверка на  не четность
            {
                win += bet * 2;
            }


            return win;

        }



    }
}
