﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasinoProject
{
    public class Casino
    {
        public static int amountbets { get; set; } = 0;                                      //количество сделанных ставок
        public static int chips { get; set; }                                          //общее количество фишек игрока
        public static bool buttonenabled = true;
        public static List<int> selectedNumbers = new List<int>();                                       //список всех выбранных чисел
        public static int number { get; private set; } = 0;                  //правильный номер выбранный машиной
        public static bool pair { get; set; } = false;                                     //ставка на четность
        public static bool impair { get; set; } = false;                                  //ставка на нечетность
        public static bool red { get; set; } = false;                                    //ставка на красный
        public static bool black { get; set; } = false;                                 //ставка на черный
        public static bool dozen1 { get; set; } = false;                               //ставка на дюжину1
        public static bool dozen2 { get; set; } = false;                              //ставка на дюжину2
        public static bool dozen3 { get; set; } = false;                             //ставка на дюжину3
        public static string login { get; set;}                   //логин
        public static string password { get; set;}                //пароль
        //public static string firstchips { get; set;} //первоначальное количество фишек
        //public static string firstspins { get; set;} //первоначальное количество спинов


        public static int SetRandNumber()                         //метод выбирает рандомно правильный номер
        {
            Random rnd = new Random();
            number = rnd.Next(0, 37);
            return number;

        }

    }
}
