﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;

namespace CasinoProject
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void ColorBox()
        {

            if (pictureBox1.BackColor == Color.Red)
                pictureBox1.BackColor = Color.Green;
            else
                pictureBox1.BackColor = Color.Red;

            if (pictureBox2.BackColor == Color.Red)
                pictureBox2.BackColor = Color.Green;
            else
                pictureBox2.BackColor = Color.Red;

            if (pictureBox3.BackColor == Color.Red)
                pictureBox3.BackColor = Color.Green;
            else
                pictureBox3.BackColor = Color.Red;

            if (pictureBox4.BackColor == Color.Red)
                pictureBox4.BackColor = Color.Green;
            else
                pictureBox4.BackColor = Color.Red;

            if (pictureBox5.BackColor == Color.Red)
                pictureBox5.BackColor = Color.Green;
            else
                pictureBox5.BackColor = Color.Red;

        }

        public void ClickButton(Button b, int n) //метод принимает кнопку и делает за нее всю работу !!!!!!!
        {
            if (b.BackColor != Color.DarkOrange)
            {
                if (CheckBet(CasinoCalculator.bet))
                {
                    b.BackColor = Color.DarkOrange;
                    CasinoCalculator.selectedNumbers.Add(n);
                    CasinoCalculator.realspin = true;


                    BetButtonEnabledFalse(); //запрещаю изменение размера ставки
                }
            }
        }

        public void ClickButton(Button b) //перегрузка метода кнопки для нечисловых ставок
        {
            if (b.BackColor != Color.DarkOrange)
            {
                if (CheckBet(CasinoCalculator.bet))
                {
                    b.BackColor = Color.DarkOrange;
                    CasinoCalculator.realspin = true;

                    BetButtonEnabledFalse(); //запрещаю изменение размера ставки
                }
            }
        }

        private void SetDefaultColors() //возвращает стандартные цвета кнопок без ставок
        {
            button2.BackColor = Color.Red;
            button3.BackColor = Color.Black;
            button4.BackColor = Color.Red;
            button5.BackColor = Color.Green;
            button6.BackColor = Color.Black;
            button7.BackColor = Color.Red;
            button8.BackColor = Color.Black;
            button9.BackColor = Color.Red;
            button10.BackColor = Color.Black;
            button11.BackColor = Color.Red;
            button12.BackColor = Color.Black;
            button13.BackColor = Color.Black;
            button14.BackColor = Color.Red;
            button15.BackColor = Color.Black;
            button16.BackColor = Color.Red;
            button17.BackColor = Color.Black;
            button18.BackColor = Color.Red;
            button19.BackColor = Color.Black;
            button20.BackColor = Color.Red;
            button21.BackColor = Color.Red;
            button22.BackColor = Color.Black;
            button23.BackColor = Color.Red;
            button24.BackColor = Color.Black;
            button25.BackColor = Color.Red;
            button26.BackColor = Color.Black;
            button27.BackColor = Color.Red;
            button28.BackColor = Color.Black;
            button29.BackColor = Color.Red;
            button30.BackColor = Color.Black;
            button31.BackColor = Color.Black;
            button32.BackColor = Color.Red;
            button33.BackColor = Color.Black;
            button34.BackColor = Color.Red;
            button35.BackColor = Color.Black;
            button36.BackColor = Color.Red;
            button37.BackColor = Color.Black;
            button38.BackColor = Color.Red;
            button39.BackColor = Color.Red;
            button40.BackColor = Color.Black;
            button41.BackColor = Color.Green;
            button42.BackColor = Color.Green;
            button43.BackColor = Color.Green;
            button44.BackColor = Color.Green;
            button45.BackColor = Color.Green;
        }

        private void BetButtonEnabledFalse()  //запрещает изменения ставок после выбранных полей ставок
        {
            button47.Enabled = false;
            button48.Enabled = false;
            button49.Enabled = false;
            button50.Enabled = false;
            button51.Enabled = false;
            button52.Enabled = false;
        }

        private void BetButtonEnabledTrue()  //разрешает изменения ставок 
        {
            button47.Enabled = true;
            button48.Enabled = true;
            button49.Enabled = true;
            button50.Enabled = true;
            button51.Enabled = true;
            button52.Enabled = true;
        }

        private bool CheckBet(int bet) //проверяет размер ставки и уменьшает на эту сумму игровой счет
        {
            Casino.chips = Convert.ToInt32(label5.Text);

            if (Casino.chips >= bet)
            {
                Casino.chips -= bet;
                label5.Text = Casino.chips.ToString();
                label2.Text = bet.ToString();
                Casino.amountbets++;
                label10.Text = Casino.amountbets.ToString();
                return true;
            }
            else
            {
                MessageBox.Show("У вас недостаточно фишек", "Ошибка!");
                return false;
            }
        }



        private void button45_Click(object sender, EventArgs e)
        {
            Casino.dozen3 = true;
            ClickButton(button45);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            ClickButton(button5, 0);

        }

        private void button46_Click(object sender, EventArgs e)
        {
            //кнопка убирает все сделанные ставки и возвращает стандартный цвет кнопок

            label2.Text = "10";
            BetButtonEnabledTrue();       //разрешает изменение ставки 
            label5.Text = (Casino.chips + Casino.amountbets * CasinoCalculator.bet).ToString(); // возвращет количество фишек до посмледней ставки
            Casino.amountbets = 0;
            label10.Text = 0.ToString();
            SetDefaultColors();       //возвращает стандартный цвет кнопок
            CasinoCalculator.realspin = false;

            Casino.red = false;
            Casino.black = false;
            Casino.dozen1 = false;
            Casino.dozen2 = false;
            Casino.dozen3 = false;
            Casino.pair = false;
            Casino.impair = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ClickButton(button2, 1);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ClickButton(button3, 2);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ClickButton(button4, 3);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            ClickButton(button6, 4);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            ClickButton(button7, 5);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            ClickButton(button8, 6);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            ClickButton(button9, 7);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            ClickButton(button10, 8);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            ClickButton(button11, 9);
        }

        private void button12_Click(object sender, EventArgs e)
        {
            ClickButton(button12, 10);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //главная кнопка запуска казино
            /////////////////////////////////////////
            Thread.Sleep(1);

            Thread thread1 = new Thread(ColorBox); //создание анимации в новом потоке
            thread1.Start();  //анимация
            thread1.Join();

            Casino.SetRandNumber(); //Случайный номер выигрыша

            if (Casino.number == 1 || Casino.number == 3 || Casino.number == 5 || //проверка на цвет числа
                Casino.number == 7 || Casino.number == 9 || Casino.number == 12 ||
                Casino.number == 14 || Casino.number == 16 || Casino.number == 18 ||
                Casino.number == 19 || Casino.number == 21 || Casino.number == 23 ||
                Casino.number == 25 || Casino.number == 27 || Casino.number == 30 ||
                Casino.number == 32 || Casino.number == 34 || Casino.number == 36)
                label1.ForeColor = Color.Red;
            else if (Casino.number == 0)
                label1.ForeColor = Color.Green;
            else
                label1.ForeColor = Color.Black;

                if (CasinoCalculator.realspin) //проверка на спины сделанные со ставкой
                {
                    CasinoCalculator.spins++;
                    label12.Text = CasinoCalculator.spins.ToString();
                    SaveInfo.Save();
            }

            label17.Text = label16.Text;
            label16.Text = label15.Text;
            label15.Text = label14.Text;
            label14.Text = label13.Text;
            label13.Text = label8.Text;
            label8.Text = label7.Text;
            label7.Text = label6.Text;
            label6.Text = label1.Text;

            label1.Text = Casino.number.ToString(); //выводит рандомный номер машины
            int win = CasinoCalculator.CalculateWin(); // выщитывает победу
            label5.Text = (Casino.chips + win).ToString(); //выводит победу

            ///////////////////
            //возвращение дефолтов:
            //////////////////

            Casino.amountbets = 0; //возвращает количество ставок к нулю
            label10.Text = 0.ToString();        //возвращает к 0 количество ставок
            Casino.selectedNumbers.Clear(); //очищает список выбранных номеров
            SetDefaultColors();            //возвращает стандартные цвета кнопок
            BetButtonEnabledTrue();       //разрешает изменение ставки 
            CasinoCalculator.realspin = false; //после нажатия кнопки добавляется спин, и запрещается дальнейший спин без ставки

            Casino.red = false; //убирает все ставки на нечисловые поля
            Casino.black = false;
            Casino.dozen1 = false;
            Casino.dozen2 = false;
            Casino.dozen3 = false;
            Casino.pair = false;
            Casino.impair = false;

            

            /////////////////////////////////////////
        }

        private void button47_Click(object sender, EventArgs e)
        {
            //ставлю 10 
            CasinoCalculator.bet = 10;
            label2.Text = 10.ToString();
            button47.BackColor = Color.LightPink;

            button48.BackColor = Color.Gray;
            button49.BackColor = Color.Gray;
            button50.BackColor = Color.Gray;
            button51.BackColor = Color.Gray;
            button52.BackColor = Color.Gray;

        }

        private void button48_Click(object sender, EventArgs e)
        {
            //ставлю 20 
            label2.Text = 20.ToString();
            CasinoCalculator.bet = 20;
            button48.BackColor = Color.LightPink;

            button47.BackColor = Color.Gray;
            button49.BackColor = Color.Gray;
            button50.BackColor = Color.Gray;
            button51.BackColor = Color.Gray;
            button52.BackColor = Color.Gray;
        }

        private void button49_Click(object sender, EventArgs e)
        {
            //ставлю 50 
            label2.Text = 50.ToString();
            CasinoCalculator.bet = 50;
            button49.BackColor = Color.LightPink;

            button47.BackColor = Color.Gray;
            button48.BackColor = Color.Gray;
            button50.BackColor = Color.Gray;
            button51.BackColor = Color.Gray;
            button52.BackColor = Color.Gray;
        }

        private void button14_Click(object sender, EventArgs e)
        {
            ClickButton(button14, 12);
        }

        private void button15_Click(object sender, EventArgs e)
        {
            ClickButton(button15, 13);
        }

        private void button16_Click(object sender, EventArgs e)
        {
            ClickButton(button16, 14);
        }

        private void button17_Click(object sender, EventArgs e)
        {
            ClickButton(button17, 15);
        }

        private void button18_Click(object sender, EventArgs e)
        {
            ClickButton(button18, 16);
        }

        private void button19_Click(object sender, EventArgs e)
        {
            ClickButton(button19, 17);
        }

        private void button20_Click(object sender, EventArgs e)
        {
            ClickButton(button20, 18);
        }

        private void button21_Click(object sender, EventArgs e)
        {
            ClickButton(button21, 19);
        }

        private void button22_Click(object sender, EventArgs e)
        {
            ClickButton(button22, 20);
        }

        private void button23_Click(object sender, EventArgs e)
        {
            ClickButton(button23, 21);
        }

        private void button24_Click(object sender, EventArgs e)
        {
            ClickButton(button24, 22);
        }

        private void button25_Click(object sender, EventArgs e)
        {
            ClickButton(button25, 23);
        }

        private void button26_Click(object sender, EventArgs e)
        {
            ClickButton(button26, 24);
        }

        private void button27_Click(object sender, EventArgs e)
        {
            ClickButton(button27, 25);
        }

        private void button28_Click(object sender, EventArgs e)
        {
            ClickButton(button28, 26);
        }

        private void button29_Click(object sender, EventArgs e)
        {
            ClickButton(button29, 27);
        }

        private void button30_Click(object sender, EventArgs e)
        {
            ClickButton(button30, 28);
        }

        private void button31_Click(object sender, EventArgs e)
        {
            ClickButton(button31, 29);
        }

        private void button32_Click(object sender, EventArgs e)
        {
            ClickButton(button32, 30);
        }

        private void button33_Click(object sender, EventArgs e)
        {
            ClickButton(button33, 31);
        }

        private void button34_Click(object sender, EventArgs e)
        {
            ClickButton(button34, 32);
        }

        private void button35_Click(object sender, EventArgs e)
        {
            ClickButton(button35, 33);
        }

        private void button36_Click(object sender, EventArgs e)
        {
            ClickButton(button36, 34);
        }

        private void button37_Click(object sender, EventArgs e)
        {
            ClickButton(button37, 35);
        }

        private void button38_Click(object sender, EventArgs e)
        {
            ClickButton(button38, 36);
        }

        private void button13_Click(object sender, EventArgs e)
        {
            ClickButton(button13, 11);
        }

        private void button39_Click(object sender, EventArgs e)
        {
            Casino.red = true;
            ClickButton(button39);
        }

        private void button40_Click(object sender, EventArgs e)
        {
            Casino.black = true;
            ClickButton(button40);
        }

        private void button43_Click(object sender, EventArgs e)
        {
            Casino.dozen1 = true;
            ClickButton(button43);
        }

        private void button44_Click(object sender, EventArgs e)
        {
            Casino.dozen2 = true;
            ClickButton(button44);
        }

        private void button41_Click(object sender, EventArgs e)
        {
            Casino.pair = true;
            ClickButton(button41);
        }

        private void button42_Click(object sender, EventArgs e)
        {
            Casino.impair = true;
            ClickButton(button42);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            button47.PerformClick(); //автоматически указываю сумму ставки по умолчанию
            label5.Text = Casino.chips.ToString();
            label12.Text = CasinoCalculator.spins.ToString();
            label18.Text ="Welcome " + Casino.login;

            
        }

        private void button50_Click(object sender, EventArgs e)
        {
            //ставлю 100 
            label2.Text = 100.ToString();
            CasinoCalculator.bet = 100;
            button50.BackColor = Color.LightPink;

            button47.BackColor = Color.Gray;
            button48.BackColor = Color.Gray;
            button49.BackColor = Color.Gray;
            button51.BackColor = Color.Gray;
            button52.BackColor = Color.Gray;
        }

        private void button51_Click(object sender, EventArgs e)
        {
            //ставлю 200 
            label2.Text = 200.ToString();
            CasinoCalculator.bet = 200;
            button51.BackColor = Color.LightPink;

            button47.BackColor = Color.Gray;
            button48.BackColor = Color.Gray;
            button49.BackColor = Color.Gray;
            button50.BackColor = Color.Gray;
            button52.BackColor = Color.Gray;

        }

        private void button52_Click(object sender, EventArgs e)
        {
            //ставлю 500 
            label2.Text = 500.ToString();
            CasinoCalculator.bet = 500;
            button52.BackColor = Color.LightPink;

            button47.BackColor = Color.Gray;
            button48.BackColor = Color.Gray;
            button49.BackColor = Color.Gray;
            button50.BackColor = Color.Gray;
            button51.BackColor = Color.Gray;
        }

        private void button53_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Рулетка - это лучшая игра для зароботка реальных денег.\n \n 1. Укажите размер ставки \n 2. Выберите все желаемые поля для ставок \n 3. Нажимайте spin и выигрывайте реальные деньги \n \n Dozen - дюжина / Odd - четный /Even - не четный \n Red - красный / Black - черный / Bet - ставка ", "Правила игры");
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
