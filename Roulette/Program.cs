﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CasinoProject
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form2());    //запускается форма регистрации 

            if(MyForms.Form2Work == false)    //проверяю закрылась ли форма регистрации в случае успешной регистрации 
            Application.Run(new Form1());    // проблема в том что форма регистрации могла закрыться и пользователем а не в случае успешной регистрации
            
           
        }
    }
}
