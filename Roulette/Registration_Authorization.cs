﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CasinoProject
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

      

        private void button1_Click(object sender, EventArgs e)
        {
            //авторизация
            string login = textBox1.Text;
            string password = textBox2.Text;
            bool probel = false; //проверка на пробел

            foreach (var item in login)
            {
                if (item == ' ')
                    probel = true;
            }

            foreach (var item in password)
            {
                if (item == ' ')
                    probel = true;
            }

            if ( probel != true && login.Length > 1 && password.Length > 1 && textBox1.Text != "" && textBox2.Text != "" && Registration_Authorization_Class.Authorization(login, password) == 0) // Проверяю на пустые поля и запускаю метод авторизации
            {
                MyForms.Form2Work = false; //перед закрытием формы 2 указываю что она закрылась корректно
                this.Close();
            }
                
             if(textBox1.Text == "" || textBox2.Text == "")
                MessageBox.Show("Заполните пустые поля.");

            if (login.Length == 1 || password.Length == 1)
                MessageBox.Show("Длина пароля или логина должна быть больше 1 символа");

            if (probel)
                MessageBox.Show("Пароль или логин не должен содержать пробел");

        }

        private void button2_Click(object sender, EventArgs e)
        {
            // регистрация
            string login = textBox1.Text;
            string password = textBox2.Text;
            bool probel = false;

            foreach (var item in login)
            {
                if (item == ' ')
                    probel = true;
            }

            foreach (var item in password)
            {
                if (item == ' ')
                    probel = true;
            }

            if ( probel != true && login.Length>1&&password.Length>1&& textBox1.Text != "" && textBox2.Text != "" && Registration_Authorization_Class.Registration(login, password) == 0) // Проверяю на пустые поля и запускаю метод авторизации
            {
                MyForms.Form2Work = false;  //перед закрытием формы 2 указываю что она закрылась корректно
                this.Close();
            }

            if (textBox1.Text == "" || textBox2.Text == "")
                MessageBox.Show("Заполните пустые поля.");

            if (login.Length == 1 || password.Length == 1)
                MessageBox.Show("Длина пароля или логина должна быть больше 1 символа");

            if (probel)
                MessageBox.Show("Пароль или логин не должен содержать пробел");
        }

        private void Form2_Load(object sender, EventArgs e)
        {
           

        }
    }
}
